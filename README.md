# Description

A repository to host design files for the fluidic system of the AcubeSAT nanosatellite, for the control of a PDMS microfluidic chip.   

Example components include:

## Manifold

A manifold that integrates all the different valves that are used to regulate the microfluidic chip operation and includes a debubbler module.

<p align="center">
  <img src=https://gitlab.com/acubesat/su/fluidics/-/raw/master/assets/manifold_schematic_updated.png width=100% height=100% />
</p>

## Chip holder

A chip holder to interface fluidic fittings, temperature sensors and a heater with the microfluidic chip.

<p align="center">
  <img src=https://gitlab.com/groups/acubesat/su/-/uploads/1d7867e2f690496a45bcfeafe80425a7/image.png width=100% height=100% />
</p>

<div align="center">
<p>
    <a href="https://benchling.com/organizations/acubesat/">Benchling 🎐🧬</a> &bull;
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf?expanded=true&viewer=rich">DDJF_PL 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>
