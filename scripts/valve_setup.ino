void setup() {
  // Define pins as outputs
  int openPin = 4;
  int closePin = 5;

  // Initialize the pins
  pinMode(openPin, OUTPUT);
  pinMode(closePin, OUTPUT);
  digitalWrite(openPin, LOW);
  digitalWrite(closePin, LOW);

  // Close valve
  digitalWrite(closePin, HIGH);
  delay(500);
  digitalWrite(closePin, LOW);

  // Wait 2 sec
  delay(2000);

  // Open valve
  digitalWrite(openPin, HIGH);
  delay(500);
  digitalWrite(openPin, LOW);

  // Wait 2 sec
  delay(2000);

  // Close valve
  digitalWrite(closePin, HIGH);
  delay(500);
  digitalWrite(closePin, LOW);

}

void loop() {
}