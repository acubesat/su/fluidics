# Logbook

# *Fluidics test*

Test performed on 5/5/2022

## **Operations**

- ### *Setup preparation*
| ID | Step Description | Remarks
| :---: | ---------------- | ---------|
| 010 | Connect the regulator to the manifold | Usage of PE tube
| 020 | Connect the overwater tube from the bottle with H2O to the first port of the manifold | Usage of 1/16 barb to male connector that was already placed on the manifold, the counting of the ports is from left to right
| 030 | Connect the air preassure gauge to the top port of the manifold | 
| 040 | Connect the underwater tube from the bottle with H2O to the threeway | Usage of 1/16 barb to male connector that was already place on the threeway
| 050 | Connect the water preassure gauge to the threeway | 
| 060 | Connect the "female luer to house barb" connector to the other side of the threeway |
| 070 | Cut approximately 70 cm of the PTFE tube  |
| 080 | Connect one side of PTFE tube to the connector on the threeway |
| 090 | Connect the other side of the PTFE tube with a "1/16 barb to male" connector|
| 100 | Connect a luer stab on the "1/16 barb to male" connector  | The setup is now complete, see attached photo "The complete setup with the threeway attached"


- ### *chip priming*
| ID | Step Description | Remarks
| :---: | ---------------- | ---------|
| 110 | Puncture a hole in an outlet of the chip with the luer stab| 
| 120 | Place the microscope over the chip  |
| 130 | Turn the microscope on |
| 140 | Place a white surface under the chip in order to receive a more clear image |
| 140 | Readjust the placement of the microscope so that the chip is centered on the image received|
| 150 | Turn on the air compressor | 
| 160 | Adjust the regulator until the preassure reaches 8 psi, indicated by the two gauges |
| 170 | Turn on the air compressor |
| 180 | Turn on the air compressor |
| 190 | Turn on the air compressor |
| 200 | Adjust the regulator to ensure air passes through |
|210| Turn on the air compressor|
| 220 | Turn on the air compressor | Not touching the chip 
| 230 | Turn on the air compressor | Not touching the chip  |
| 240 | Turn on the air compressor till the preassure is 2 psi |
| 250 | Air pump turned on (2 psi) |
| 260 | Water allowed to flow through chip|
|270| Water fills chip  |
|280| Full set up (with three-way) reassembled with new chip |
|290| Pumped turn on ( 3 psi ) |
|300| Set up dissasembled |
|310| Set up carefully moved into freezer, along with a thermocouple placed earlier | The setup consisted of the three-way, on which the gauge and the luer stab, the chip and the plastic box




## **Results**
- ### *Setup preparation*
|   ID   |       Result      | Status |
| :----: | ----------------- | :------: |
|  010  | Regulator is connected to the manifold through the PE tube | ![complete]
|  020  | The overwater tube from the bottle is connected to the manifold | ![complete]
|  030  | The air preassure gauge is connected on the manifold | ![complete]
|  040  | The underwater tube from the bottle is connected to the threeway | ![complete]
|  050  | The water preassure gauge is connected to the threeway | ![complete]
|  060  | The "female luer to house barb" connector is attached onto the threeway | ![complete]
|  070  | Approximately 70cm of the PTFE tube is cut | ![complete]
|  080  | One side of the PTFE tube is connected on the connector on the threeway | ![complete]
|  090  | The other side of the PTFE tube is connected to a "1/16 barb to male" connector | ![complete]
|  100  | A luer stab is connected on the "1/16 barb to male" connector | ![complete]


- ### *chip priming*
|   ID   |       Result      | Status |
| :----: | ----------------- | :------: |
|  110  | The chip has been punctured but the setup is not stable and is falling over, leads to deviation 111 | ![deviation]
|  120  | Microscope is placed over  the chip | ![complete]
|  130  | Microscope is turned on| ![complete]
|  140  | White surface is placed under the chip | ![complete]
|  150  | Air compressor is turned on | ![complete]
|  160  | Air doesn't pass through the setup , leads to deviation 161 | ![deviation]
|  170  | The chip has cracked, leads to deviation 171 | ![deviation]
|  180  |  The chip has cracked, leads to deviation 181 | ![deviation]
|  190  | Air compressor is on  | ![complete]
|  200  | Gauges show no measurements, but water is forming on the chip droping from the inlet, leads to deviation 201| ![deviation]
|  210  | No water is going into the chip, leads to deviation 211  | ![deviation]
|  220  | Water ran out, dev 231 | ![deviation]
|  230  | Water does not pass through, dev 231 | ![deviation]
|  240  | Preassure is 2 psi, tube from the bottle to the 3way disconnected, leads to deviation 241 | ![deviation]
|  250  | Water leaking from inlet, leads to deviation 251 | ![deviation]
| 260 |Pump turned on | ![complete]
| 270| Water fills chip | ![complete]
|280| Full set up (with three-way) reassembled with new chip |![complete]
|290| Water fills the chip, Water leakege from the stab tip, leads to deviation 291 | ![deviation]
|300| Set up dissasembled | ![complete]
|310| Set up succesfully placed in the freezer | ![complete]

## **Deviations**

### **ID-110**

|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  111  | A new piece of PTFE tube, of approximately 4cm, is now used to connect the threeway and the luer stab on a new chip.  | ![successful]
|  112  | The chip is replaced with a new one | ![successful]
|  113  | A 3D printed box is used to hold the luer stab in place, by balancing the water preassure gauge onto the top of it | ![successful]
|  114  | The setup is now stable, move to step 120 (see atached photo "the complete setup with the threeway attached") | ![successful]
### **ID-160**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  161  | move the overwater tube from the first port of the manifold to the second from last one | ![successful]
|  162  | move the air preassure gauge from the top port of the manifold to the last | ![successful]
|  163  | The chip has cracked | ![anomaly]
|  164  | Inspect the new chip with the microscope  | ![successful]
|  165  | Chip has been replaced, move to step 170 | ![successful]
### **ID-170**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  171  | Chip is replaced with a new one, move to step 180 | ![successful]
### **ID-180**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  181  | Chip is replaced with a new one which is flow, move to step 190| ![successful]
### **ID-200**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  201  | Open and close some ports | ![successful]
|  202  | Gauges show measurements | ![successful]
|  203  | Water does not pass through  | ![anomaly]
| 204  | Disconnect the luer stab from the chip| ![successful]
| 205 | Chip is replaced with a new one | ![successful]
| 206 | Luer stab is replaced with a new one, move to step 210| ![successful]
### **ID-210**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  211  | Disconnect the luer stab from the chip| ![successful]
| 212 | Chip is replaced with a new one, move to step 220 | ![successful]
### **ID-220**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
| 221  | replace the water bottle and move to step 230| ![successful]
### **ID-230**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  231 | Replace the luer stab with the previous one| ![successful]
|  232 | Luer stab is placed on the chip, move to step 240 | ![successful]
### **ID-240**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  241 | Three-way reconnected | ![successful]
|  242 | Set up reassembled, move to step 250 | ![successful]
### **ID-250**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
|  251 | More pressure added | ![successful]
|  252 | Three-way dismantled and water removed | ![successful]
|  253 | New chip installed, without the 3-way, see attached photo "The setup without the threeway" | ![successful]
|  254 |A maximum of 5 psi  used to pump water, chip does not fill with water | ![anomaly]
|  255 |Leuer stab removed from set up, leuer stab was the reason of anomaly | ![successful]
|  256 |New luer stab installed, being larger than the ones the chip was designed for | ![successful]
|  257 |set up without 3 way reassembled, move to step 260 | ![successful]
### **ID-290**
|   ID   |       Step Description      | Status |
| :----: | --------------------------- | ------ |
| 291 | Water leakage had to be cleaned from the chip, move to step 300| ![successful]

# Photos
- The complete setup with the threeway attached: ![setup_with_threeway]
- The setup without the threeway attached: ![setup_without_threeway]




# Extra
- The reason behind the failure of some of the chip priming attemps is that the water gets in between the chip and the glass due to the bonding process that was incomplete.


[complete]: <./images/markdown_complete.png>
[successful]: <./images/markdown_successful.png>
[deviation]: <./images/markdown_deviation.png>
[anomaly]: <./images/markdown_anomaly.png>
[setup_with_threeway]: <./images/setup_with_threeway.jpg>
[setup_without_threeway]: <./images/setup_without_threeway.jpg>